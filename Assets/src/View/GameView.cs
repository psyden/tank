using System.Collections.Generic;
using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;
using Tanks.UI;
using Tanks.View.Systems;
using Tanks.View.UI;
using UnityEngine;

namespace Tanks.View
{
    public class GameView
    {
        private Scheduler _scheduler;
        private GameController _controller;
        private PrefabsLoader _prefabsLoader;
        private CanvasController _canvas;
        
        private ViewCollections _viewCollections;
        private SpawningManager _spawningManager;
        private ModelRequestsHandler _modelRequestsHandler;
        private InputManager _inputManager;

        private UiFactory _uiFactory;
        private List<BaseSystem> _systems;
        private UiScreensManager _uiScreenManager;
        private CollisionsManager _collisionsManager;

        public void Init(GameController controller, PrefabsLoader prefabsLoader, Scheduler scheduler, CanvasController canvas)
        {
            _controller = controller;
            _scheduler = scheduler;
            _prefabsLoader = prefabsLoader;
            _canvas = canvas;
        }

        private void CreateManagers()
        {
            _uiScreenManager = new UiScreensManager();
            _uiScreenManager.Init(_uiFactory, _canvas);

            _collisionsManager = new CollisionsManager();
            _collisionsManager.Init(_viewCollections, _controller);
        }

        private void CreateFactories()
        {
            _uiFactory = new UiFactory();
            _uiFactory.Init(_prefabsLoader);
        }

        private void CreateSystems()
        {
            var movementSystem = new MovementSystem();
            movementSystem.Init(_inputManager, _viewCollections);

            var shootingSystem = new ShootingSystem();
            shootingSystem.Init(_inputManager, _viewCollections, _controller, _prefabsLoader);
            
            var damageViewSystem = new DamageViewSystem();
            damageViewSystem.Init(_viewCollections, _spawningManager, _uiScreenManager, _controller);
            
            var uiSystem = new UiSystem();
            uiSystem.Init(_uiScreenManager, _viewCollections);
            
            _systems = new List<BaseSystem>
            {
                movementSystem,
                shootingSystem,
                damageViewSystem,
                uiSystem
            };
            
            for (int i = 0; i < _systems.Count; i++)
            {
                var system = _systems[i];
                system.Start();
                if (system is IUpdate iUpdateSystem)
                {
                    _scheduler.AddUpdatable(iUpdateSystem);
                }
                if (system is ILateUpdate iLateUpdateSystem)
                {
                    _scheduler.AddUpdatable(iLateUpdateSystem);
                }
                if (system is IFixedUpdate iFixedUpdateSystem)
                {
                    _scheduler.AddUpdatable(iFixedUpdateSystem);
                }
            }
        }

        private InputManager CreateInputManager()
        {
            var inputManagerGO = new GameObject("InputManager");
            var inputManager = inputManagerGO.AddComponent<InputManager>();
            return inputManager;
        }

        public void Start()
        {
            _inputManager = CreateInputManager();

            _viewCollections = new ViewCollections();
            _viewCollections.Init();
            
            _spawningManager = new SpawningManager();
            _spawningManager.Init(_viewCollections, _prefabsLoader);
            
            CreateFactories();
            CreateManagers();
            CreateSystems();
            
            _modelRequestsHandler = new ModelRequestsHandler();
            _modelRequestsHandler.Init(_controller, _spawningManager, _collisionsManager);
            
            _scheduler.AddUpdatable(_modelRequestsHandler);
            _scheduler.AddUpdatable(_inputManager);

            _controller.GameOverView += OnGameOver;
        }

        private void OnGameOver()
        {
            Stop();
        }

        public void Stop()
        {
            _controller.GameOverView -= OnGameOver;
            
            for (int i = 0; i < _systems.Count; i++)
            {
                var system = _systems[i];
                system.Stop();
                if (system is IUpdate iUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iUpdateSystem);
                }
                if (system is ILateUpdate iLateUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iLateUpdateSystem);
                }
                if (system is IFixedUpdate iFixedUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iFixedUpdateSystem);
                }
            }
            
            _scheduler.RemoveUpdatable(_modelRequestsHandler);
            _scheduler.RemoveUpdatable(_inputManager);
            
            _uiScreenManager.Deinit();
            _collisionsManager.Deinit();
            _spawningManager.Deinit();
            _modelRequestsHandler.Deinit();

            var gameOverScreen = _uiFactory.CreateGameOverScreen();
            gameOverScreen.transform.SetParent(_canvas.RectTransform, false);
            
        }
    }
}