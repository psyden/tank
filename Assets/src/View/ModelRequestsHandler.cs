using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;

namespace Tanks.View
{
    public class ModelRequestsHandler : IFixedUpdate
    {
        private GameController _gameController;
        
        private SpawningManager _spawningManager;
        private CollisionsManager _collisionsManager;

        public void Init(GameController gameController, SpawningManager spawningManager, 
            CollisionsManager collisionsManager)
        {
            _gameController = gameController;

            _spawningManager = spawningManager;
            _collisionsManager = collisionsManager;
        }

        public void Deinit()
        {
            _collisionsManager.ManageSubscriptions(false);
        }

        public void OnFixedUpdate(DeltaTimeStruct dt)
        {
            ManageUnitSpawns();
        }

        void ManageUnitSpawns()
        {
            var unitSpawns = _gameController.GetUnitSpawns();
            for (int i = 0; i < unitSpawns.Count; i++)
            {
                var item = unitSpawns.GetAndUse(i);
                _spawningManager.SpawnUnit(item);

                if (item.UnitTypeName == UnitTypeNames.Player)
                {
                    _collisionsManager.ManageSubscriptions(true);
                }
            }

            unitSpawns.Clear();
        }
    }
}