using System.Collections.Generic;
using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View.Factory
{
    public class BulletViewFactory
    {
        protected PrefabsLoader PrefabsLoaderInstance;

        protected Dictionary<string, InstancePool<GameObject>> Pools;

        protected Vector3 BulletSpawnOffset = Vector3.up * 1.5f;

        public BulletViewFactory()
        {
            Pools = new Dictionary<string, InstancePool<GameObject>>();
        }

        public void Init(PrefabsLoader prefabsLoader)
        {
            PrefabsLoaderInstance = prefabsLoader;
        }

        public BulletView Create(int id, string weaponTypeName, Vector3 position, Vector3 direction)
        {
            string bulletTypeName = SelectBulletByWeapon(weaponTypeName);

            InstancePool<GameObject> pool = GetPool(weaponTypeName);

            GameObject bulletGO = null;
            if (pool.Count > 0)
            {
                bulletGO = GetFromPool(weaponTypeName);
                bulletGO.transform.position = position + BulletSpawnOffset;
            }
            else
            {
                var bulletPrefab = PrefabsLoaderInstance.GetPrefab(bulletTypeName);
                bulletGO = GameObject.Instantiate(bulletPrefab, position + BulletSpawnOffset, Quaternion.identity);
            }
            
            bulletGO.transform.forward = direction;
            var bulletView = bulletGO.GetComponent<BulletView>();

            var weaponCharacteristics = AllGameConfigs.CharacteristicsCfg.GetWeapon(weaponTypeName).GetClone();
            
            var movement = new StraightBulletMovement();
            movement.Init(bulletView.transform, weaponCharacteristics);
            
            bulletView.Init(id, weaponTypeName, movement);

            return bulletView;
        }

        private string SelectBulletByWeapon(string weaponTypeName)
        {
            string bulletTypeName = null;
            switch (weaponTypeName)
            {
                case WeaponTypeNames.WeaponFast:
                {
                    bulletTypeName = BulletTypeNames.WeaponFastBullet;
                    break;
                }
                case WeaponTypeNames.WeaponHeavy:
                {
                    bulletTypeName = BulletTypeNames.WeaponHeavyBullet;
                    break;
                }
                default:
                {
                    bulletTypeName = BulletTypeNames.WeaponFastBullet;
                    break;
                }
            }

            return bulletTypeName;
        }

        private InstancePool<GameObject> GetPool(string weaponTypeName)
        {
            string bulletTypeName = SelectBulletByWeapon(weaponTypeName);
            InstancePool<GameObject> pool = null;
            if (!Pools.TryGetValue(bulletTypeName, out pool))
            {
                pool = new InstancePool<GameObject>();
                Pools[bulletTypeName] = pool;
            }

            return pool;
        }

        public void PoolInstance(string weaponTypeName, GameObject unit)
        {
            InstancePool<GameObject> pool = GetPool(weaponTypeName);

            unit.transform.position = Vector3.down*100;
            pool.Push(unit);
            unit.SetActive(false);
        }

        public GameObject GetFromPool(string weaponTypeName)
        {
            InstancePool<GameObject> pool = GetPool(weaponTypeName);;

            GameObject instance = null;
            if (pool.Count > 0)
            {
                instance = pool.Pop();
                instance.SetActive(true);
            }

            return instance;
        }
    }
}