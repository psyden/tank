using Tanks.Root;
using UnityEngine;

namespace Tanks.View.Factory
{
    public class EnemyViewFactory : BaseUnitViewFactory
    {
        public EnemyView Create(int id, string unitTypeName, Vector3 position)
        {
            GameObject enemyGO = null;
            
            var pool = GetPool(unitTypeName);
            if (pool.Count > 0)
            {
                enemyGO = GetFromPool(unitTypeName);
            }
            else
            {
                var enemy = PrefabsLoaderInstance.GetPrefab(unitTypeName);
                enemyGO = GameObject.Instantiate(enemy);
            }
            
            var enemyView = enemyGO.GetComponent<EnemyView>();
            var data = AllGameConfigs.CharacteristicsCfg.GetUnit(unitTypeName);
            var characteristics = data.GetClone();
            characteristics.IsEnemy = true;
            enemyView.Init(id, characteristics);

            enemyView.NavAgent.speed = characteristics.Speed;
            enemyView.NavAgent.angularSpeed = characteristics.RotationSpeed;
            enemyView.NavAgent.Warp(position);
            
            return enemyView;
        }
    }
}