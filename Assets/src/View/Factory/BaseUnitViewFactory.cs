using System.Collections.Generic;
using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View.Factory
{
    public class BaseUnitViewFactory
    {
        protected PrefabsLoader PrefabsLoaderInstance;
        
        protected Dictionary<string, InstancePool<GameObject>> Pools;

        public BaseUnitViewFactory()
        {
            Pools = new Dictionary<string, InstancePool<GameObject>>();
        }

        public void Init(PrefabsLoader prefabsLoader)
        {
            PrefabsLoaderInstance = prefabsLoader;
        }
        
        protected InstancePool<GameObject> GetPool(string unitTypeName)
        {
            InstancePool<GameObject> pool = null;
            if (!Pools.TryGetValue(unitTypeName, out pool))
            {
                pool = new InstancePool<GameObject>();
                Pools[unitTypeName] = pool;
            }

            return pool;
        }

        public void PoolInstance(string unitTypeName, GameObject unit)
        {
            InstancePool<GameObject> pool = GetPool(unitTypeName);

            unit.transform.position = Vector3.down*100;
            pool.Push(unit);
            unit.SetActive(false);
        }

        public GameObject GetFromPool(string unitTypeName)
        {
            InstancePool<GameObject> pool = GetPool(unitTypeName);;

            GameObject instance = null;
            if (pool.Count > 0)
            {
                instance = pool.Pop();
                instance.SetActive(true);
            }

            return instance;
        }
    }
}