using Tanks.Root;
using UnityEngine;

namespace Tanks.View.Factory
{
    public class PlayerViewFactory : BaseUnitViewFactory
    {
        public PlayerView Create(int id, string unitTypeName, Vector3 position)
        {
            var player = PrefabsLoaderInstance.GetPrefab(unitTypeName);
            var playerGO = GameObject.Instantiate(player, position, Quaternion.identity);
            var playerView = playerGO.GetComponent<PlayerView>();

            var data = AllGameConfigs.CharacteristicsCfg.GetUnit(unitTypeName);
            var characteristics = data.GetClone();
            characteristics.IsEnemy = false;
            playerView.Init(id, characteristics);
            
            return playerView;
        }
    }
}