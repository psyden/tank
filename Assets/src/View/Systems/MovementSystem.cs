using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View.Systems
{
    public class MovementSystem : BaseSystem, IFixedUpdate
    {
        private InputManager _inputManager;
        private ViewCollections _viewCollections;
        private float _gameFieldRadius;
        private float _gameFieldRadiusSqr;

        public void Init(InputManager inputManager, ViewCollections viewCollections)
        {
            _inputManager = inputManager;
            _viewCollections = viewCollections;

            _gameFieldRadius = AllGameConfigs.BattleCfg.GameFieldRadius;
            _gameFieldRadiusSqr = _gameFieldRadius * _gameFieldRadius;
        }

        public void OnFixedUpdate(DeltaTimeStruct dt)
        {
            var playerView = _viewCollections.Player;
            
            if (playerView != null)
            {
                var inputData = _inputManager.CurrentInputData;
                MoveAndRotatePlayer(playerView, inputData, dt);
                
                foreach (var enemyView in _viewCollections.Enemies)
                {
                    var navAgent = enemyView.NavAgent;
                    navAgent.SetDestination(playerView.transform.position);

                    var speedPercent = navAgent.speed / enemyView.Characteristics.Speed;
                    enemyView.UpdateAnimation(speedPercent);
                }
            }
        }

        private void MoveAndRotatePlayer(PlayerView playerView, InputData inputData, DeltaTimeStruct dt)
        {
            var playerTransform = playerView.transform;
                
            var characteristics = playerView.Characteristics;
            var playerSpeed = characteristics.Speed;
            var playerRotationSpeed = characteristics.RotationSpeed;

            var nextForward = CalculateNextForward(playerView, playerRotationSpeed, inputData, dt);
            Vector3 deltaMovement = CalculateDeltaMovement(inputData, nextForward, playerSpeed, dt);

            Vector3 position = playerView.Rb.position;
            
            bool isAchieved = CheckGameFieldBounds(position, deltaMovement);
            
            playerTransform.forward = nextForward;
            
            if (!isAchieved)
            {
                playerView.Rb.position += deltaMovement;
            }
        }

        private Vector3 CalculateNextForward(UnitView unitView, float playerRotationSpeed, InputData inputData,
            DeltaTimeStruct dt)
        {
            var inputRotationAxis = (inputData.Vertical >= 0) ? inputData.Horizontal : -inputData.Horizontal;
            
            var unitTransform = unitView.transform;
            var currentForward = unitTransform.forward;

            var rotation = Quaternion.AngleAxis(playerRotationSpeed * dt.Delta * inputRotationAxis, Vector3.up);
            var nextForward = rotation * currentForward;
            
            return nextForward;
        }

        private Vector3 CalculateDeltaMovement(InputData inputData, Vector3 forward, float speed, DeltaTimeStruct dt)
        {
            var forwardInput = inputData.Vertical;

            Vector3 deltaMovement = forwardInput * speed * dt.Delta * forward;

            return deltaMovement;
        }
        
        private bool CheckGameFieldBounds(Vector3 position, Vector3 deltaMovement)
        {
            var sqrMagnitude = (position + deltaMovement - Vector3.zero).sqrMagnitude;
            bool isAchieved = (sqrMagnitude > 0.1) ? sqrMagnitude >= _gameFieldRadiusSqr : false;
            return isAchieved;
        }
        
    }
}