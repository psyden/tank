using System.Collections.Generic;
using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;
using Tanks.View.Factory;
using UnityEngine;

namespace Tanks.View.Systems
{
    public class ShootingSystem : BaseSystem, IUpdate, IFixedUpdate
    {
        private InputManager _inputManager;
        private ViewCollections _viewCollections;
        private GameController _gameController;
        private PrefabsLoader _prefabsLoader;
        private BulletViewFactory _bulletViewFactory;

        public void Init(InputManager inputManager, ViewCollections viewCollections, GameController gameController,
            PrefabsLoader prefabsLoader)
        {
            _inputManager = inputManager;
            _viewCollections = viewCollections;
            _gameController = gameController;
            _prefabsLoader = prefabsLoader;

            _bulletViewFactory = new BulletViewFactory();
            _bulletViewFactory.Init(prefabsLoader);            
        }

        public void OnUpdate(DeltaTimeStruct dt)
        {
            var player = _viewCollections.Player;

            if (player != null)
            {
                var prevInputData = _inputManager.PrevInputData;
                var currentInputData = _inputManager.CurrentInputData;

                if (prevInputData.Shoot != currentInputData.Shoot)
                {
                    var playerTransform = player.transform;
                    var position = playerTransform.position;
                    var direction = playerTransform.forward;
                    _gameController.RegisterShootButtonChange(currentInputData.Shoot, position, direction);
                }

                if (currentInputData.NextWeapon || currentInputData.PrevWeapon)
                {
                    bool switchingValue = (currentInputData.NextWeapon) ? true : false;
                    _gameController.RegisterChangeWeaponRequest(switchingValue);
                }
            }
        }

        public void OnFixedUpdate(DeltaTimeStruct dt)
        {
            CreateBullets();
            MoveBullets(dt);
        }

        private void CreateBullets()
        {
            var shootAnswers = _gameController.GetShootAnswers();

            for (int i = 0; i < shootAnswers.Count; i++)
            {
                var data = shootAnswers[i];

                if (data.Shoot)
                {
                    var id = data.Id;
                    var position = data.Position;
                    var direction = data.Direction;
                    var weaponTypeName = data.WeaponTypeName;

                    var bulletView = _bulletViewFactory.Create(id, weaponTypeName, position, direction);
                    _viewCollections.Bullets.Add(id, bulletView);

                    ActivateBulletSubscriptions(bulletView, true);
                }
            }
            
            shootAnswers.Clear();
        }
        
        private void MoveBullets(DeltaTimeStruct dt)
        {
            foreach (BulletView bullet in _viewCollections.Bullets)
            {
                bullet.BulletMovement.Tick(dt);
            }
        }

        private Collider[] _overlapCast = new Collider[10];
        void OnBulletCollided(int bulletId, int enemyId, Vector3 position)
        {
            var bulletView = _viewCollections.Bullets.Get(bulletId);
            
            ActivateBulletSubscriptions(bulletView, false);
            
            var weaponTypeName = bulletView.WeaponTypeName;

            var weaponCfg = AllGameConfigs.CharacteristicsCfg.GetWeapon(weaponTypeName);
            var damageRadius = weaponCfg.Characteristics.DamageRadius;

            var layer = 1 << LayerMask.NameToLayer(LayerName.Character);
            Physics.OverlapSphereNonAlloc(position, damageRadius, _overlapCast, layer);

            var enemyDamaged = new List<int>(10);
            for (int i = 0; i < _overlapCast.Length; i++)
            {
                Collider collider = _overlapCast[i];
                if (collider != null)
                {
                    var enemyView = collider.GetComponent<UnitView>();
                    if (enemyView != null)
                    {
                        enemyDamaged.Add(enemyView.Id);
                    }
                }
            }
            
            _gameController.RegisterBulletHits(bulletId, weaponTypeName, enemyDamaged);

            _viewCollections.Bullets.Remove(bulletId);
            bulletView.Deinit();
            _bulletViewFactory.PoolInstance(bulletView.WeaponTypeName, bulletView.gameObject);
        }

        private void OnBulletWallCollided(int bulletId)
        {
            var bulletView = _viewCollections.Bullets.Get(bulletId);
            ActivateBulletSubscriptions(bulletView, false);
            bulletView.Deinit();
            _viewCollections.Bullets.Remove(bulletId);
            _bulletViewFactory.PoolInstance(bulletView.WeaponTypeName, bulletView.gameObject);
        }

        private void ActivateBulletSubscriptions(BulletView bulletView, bool isActive)
        {
            if (isActive)
            {
                bulletView.OnCollided += OnBulletCollided;
                bulletView.OnWallCollided += OnBulletWallCollided;
            }
            else
            {
                bulletView.OnCollided -= OnBulletCollided;
                bulletView.OnWallCollided -= OnBulletWallCollided;
            }
        }
    }
}