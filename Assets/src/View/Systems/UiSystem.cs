using Tanks.Common;
using Tanks.Root;
using Tanks.View.UI;
using UnityEngine;

namespace Tanks.View.Systems
{
    public class UiSystem : BaseSystem, ILateUpdate
    {
        private UiScreensManager _uiScreensManager;
        private ViewCollections _viewCollections;

        public void Init(UiScreensManager uiScreensManager, ViewCollections viewCollections)
        {
            _uiScreensManager = uiScreensManager;
            _viewCollections = viewCollections;
        }

        public void Deinit()
        {
            
        }

        public void OnLateUpdate(DeltaTimeStruct dt)
        {
            var healthBarScreen = _uiScreensManager.HealthBarScreen;
            foreach (var unitView in _viewCollections.Enemies)
            {
                ProcessUnit(unitView, healthBarScreen);
            }

            if (_viewCollections.Player != null)
            {
                ProcessUnit(_viewCollections.Player, healthBarScreen);
            }
        }

        void ProcessUnit(UnitView unitView, HealthBarScreen healthBarScreen)
        {
            var unitId = unitView.Id;
            var characteristics = unitView.Characteristics;
            var health = characteristics.Health;
            var healthMax = characteristics.HealthMax;
                
            var hpBarWorldPos = unitView.transform.TransformPoint(Vector3.up * 3);

            var position = GameMath.WorldToCanvasPosition(hpBarWorldPos, _uiScreensManager.Camera,
                _uiScreensManager.HealthBarScreen.RectTransform);
                
            healthBarScreen.UpdateView(unitId, health, healthMax, position);
        }
    }
}