using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;
using Tanks.View.UI;
using UnityEngine;

namespace Tanks.View.Systems
{
    public class DamageViewSystem : BaseSystem, IFixedUpdate
    {
        private ViewCollections _collections;
        private SpawningManager _spawningManager;
        private UiScreensManager _uiScreensManager;
        private GameController _gameController;

        public void Init(ViewCollections collections, SpawningManager spawningManager,
            UiScreensManager uiScreensManager, GameController gameController)
        {
            _collections = collections;
            _spawningManager = spawningManager;
            _gameController = gameController;
            _uiScreensManager = uiScreensManager;
        }

        public void OnFixedUpdate(DeltaTimeStruct dt)
        {
            var damageList = _gameController.GetDamage();

            for (int i = 0; i < damageList.Count; i++)
            {
                var data = damageList[i];
                var unitId = data.UnitId;
                var unitType = data.UnitType;

                if (unitType == UnitTypeEnum.Player)
                {
                    var playerView = _collections.Player;
                    playerView.Characteristics.Health = data.Health;
                }
                else if (unitType == UnitTypeEnum.Enemy)
                {
                    var enemyView = _collections.Enemies.Get(unitId);

                    if (enemyView != null)
                    {
                        enemyView.Characteristics.Health = data.Health;

                        if (data.IsDead)
                        {
                            enemyView.Deinit();
                            _collections.Enemies.Remove(unitId);
                            _spawningManager.EnemyViewFactory.PoolInstance(enemyView.Characteristics.UnitTypeName, enemyView.gameObject);
                        
                            _uiScreensManager.HealthBarScreen.RemoveView(unitId);
                        }
                    }
                }

                
            }
            
            damageList.Clear();
        }
    }
}