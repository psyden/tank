using Tank.Common;

namespace Tanks.View
{
    public class ViewCollections
    {
        private PlayerView _player;
        public PlayerView Player
        {
            get => _player;
            set => _player = value;
        }
        
        private BaseCollection<EnemyView> _enemies;
        public BaseCollection<EnemyView> Enemies => _enemies;

        private BaseCollection<BulletView> _bullets;
        public BaseCollection<BulletView> Bullets => _bullets;

        public void Init()
        {
            _enemies = new BaseCollection<EnemyView>();
            _bullets = new BaseCollection<BulletView>();
        }
        
    }
}