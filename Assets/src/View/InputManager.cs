using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View
{
    public class InputManager : MonoBehaviour, IUpdate
    {
        private InputData _prevInputData;
        public InputData PrevInputData => _prevInputData;

        private InputData _currentInputData;
        public InputData CurrentInputData => _currentInputData;

        public void OnUpdate(DeltaTimeStruct dt)
        {
            var vertical = Input.GetAxis("Vertical");
            var horizontal = Input.GetAxis("Horizontal");

            bool shoot = Input.GetButton(KeysNames.Shoot); 
            
            if (Input.GetButtonUp(KeysNames.Shoot))
            {
                shoot = false;
            }

            bool nextWeapon = Input.GetButtonDown(KeysNames.NextWeapon);
            bool prevWeapon = Input.GetButtonDown(KeysNames.PrevWeapon);

            _prevInputData = _currentInputData;
            
            _currentInputData = new InputData
            {
                Vertical = vertical,
                Horizontal = horizontal,
                Shoot = shoot,
                NextWeapon = nextWeapon,
                PrevWeapon = prevWeapon
            };
        }
    }

    public struct InputData
    {
        public float Vertical;
        public float Horizontal;
        public bool Shoot;
        public bool PrevWeapon;
        public bool NextWeapon;
    }
    
    public static class KeysNames
    {
        public const string Shoot = "Shoot";
        public const string NextWeapon = "NextWeapon";
        public const string PrevWeapon = "PrevWeapon";
    }
}