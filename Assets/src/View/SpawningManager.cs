using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;
using Tanks.View.Factory;

namespace Tanks.View
{
    public class SpawningManager
    {
        private ViewCollections _collections;
        private PrefabsLoader _prefabsLoader;
        private PlayerViewFactory _playerViewFactory;
        private EnemyViewFactory _enemyViewFactory;

        public PlayerViewFactory PlayerViewFactory => _playerViewFactory;
        public EnemyViewFactory EnemyViewFactory => _enemyViewFactory;

        public void Init(ViewCollections collections, PrefabsLoader prefabsLoader)
        {
            _collections = collections;
            _prefabsLoader = prefabsLoader;
            
            _playerViewFactory = new PlayerViewFactory();
            _playerViewFactory.Init(_prefabsLoader);
            
            _enemyViewFactory = new EnemyViewFactory();
            _enemyViewFactory.Init(_prefabsLoader);
        }

        public void Deinit() {
        }

        public void SpawnUnit(RegisterUnitSpawnStruct item)
        {
            var id = item.Id;
            var factoryType = item.FactoryType;
            var typeName = item.UnitTypeName;
            var position = item.Position;

            switch (factoryType)
            {
                case UnitFactoryType.Player:
                {
                    var playerView = _playerViewFactory.Create(id, typeName, position);
                    _collections.Player = playerView;
                    break;
                }
                case UnitFactoryType.Enemy:
                {
                    var enemyView = _enemyViewFactory.Create(id, typeName, position);
                    _collections.Enemies.Add(id, enemyView);
                    break;
                }
            }
        }

    }
}