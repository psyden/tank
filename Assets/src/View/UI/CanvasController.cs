﻿using UnityEngine;

namespace Tanks.UI
{
    public class CanvasController : MonoBehaviour
    {

        [SerializeField]
        private RectTransform _rectTransform;
        public RectTransform RectTransform => _rectTransform;
    }
}