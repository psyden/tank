using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View.UI
{
    public class UiFactory
    {
        private PrefabsLoader _prefabsLoader;

        private InstancePool<GameObject> _pool;

        public UiFactory()
        {
            _pool = new InstancePool<GameObject>();
        }

        public void Init(PrefabsLoader prefabsLoader)
        {
            _prefabsLoader = prefabsLoader;
        }

        public HealthBar CreateHealthBar(int id, string prefabName)
        {
            GameObject instance = null;
            
            if (_pool.Count > 0)
            {
                instance = _pool.Pop();
                instance.SetActive(true);
            }
            else
            {
                var prefab = _prefabsLoader.GetPrefab(prefabName);
                instance = GameObject.Instantiate(prefab);
            }
            
            var healthBar = instance.GetComponent<HealthBar>();
            healthBar.Init(id);

            return healthBar;
        }

        public void PoolHealthBar(HealthBar bar)
        {
            _pool.Push(bar.gameObject);
            bar.gameObject.SetActive(false);
        }

        public HealthBarScreen CreateHealthBarScreen()
        {
            var prefab = _prefabsLoader.GetPrefab(UIPrefabsNames.HealthScreen);
            var instance = GameObject.Instantiate(prefab);
            var healthBarScreen = instance.GetComponent<HealthBarScreen>();

            return healthBarScreen;
        }

        public GameObject CreateGameOverScreen()
        {
            var prefab = _prefabsLoader.GetPrefab(UIPrefabsNames.GameOverScreen);
            var instance = GameObject.Instantiate(prefab);

            return instance;
        }
        
    }

    
}