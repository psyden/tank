﻿using UnityEngine;
using UnityEngine.UI;

namespace Tanks.View.UI
{

    public class HealthBar : MonoBehaviour
    {

        [SerializeField]
        private RectTransform _rectTransform;
        
        [SerializeField]
        private Image _fillImage;
        
        [SerializeField]
        private Text _text;

        private int _id;
        public int Id => _id;
        
        public void Init(int id)
        {
            _id = id;
        }

        public void Deinit()
        {
        }

        public void UpdateView(float current, float max, Vector2 position)
        {
            float percent = current / max;

            _fillImage.fillAmount = percent;
            _text.text = current.ToString() + " / " + max.ToString();

            _rectTransform.anchoredPosition = position;
        }
    }

}