﻿using System.Collections.Generic;
using Tanks.Root;
using Tanks.UI;
using UnityEngine;

namespace Tanks.View.UI
{

    public class HealthBarScreen : MonoBehaviour
    {

        [SerializeField]
        private RectTransform _rectTransform;
        public RectTransform RectTransform => _rectTransform;

        private Dictionary<int, HealthBar> _bars = new Dictionary<int, HealthBar>();
        
        private UiFactory _uiFactory;
        private CanvasController _canvas;

        public void Init(UiFactory uiFactory, CanvasController canvas)
        {
            _uiFactory = uiFactory;
            _canvas = canvas;
        }

        public void Deinit()
        {
            foreach (var entry in _bars)
            {
                var bar = entry.Value;
                bar.Deinit();
                _uiFactory.PoolHealthBar(bar);
            }
            
            _bars.Clear();
        }

        public void UpdateView(int unitId, float current, float max, Vector2 position)
        {
            HealthBar bar = GetHealthBar(unitId);
            bar.UpdateView(current, max, position);
        }

        public void RemoveView(int unitId)
        {
            if (_bars.TryGetValue(unitId, out var healthBar))
            {
                healthBar.Deinit();
                _bars.Remove(unitId);
                _uiFactory.PoolHealthBar(healthBar);
            }
        }
        
        private HealthBar GetHealthBar(int unitId)
        {
            HealthBar bar = null;
            if (!_bars.TryGetValue(unitId, out bar))
            {
                bar = _uiFactory.CreateHealthBar(unitId, UIPrefabsNames.HealthBar);
                bar.transform.SetParent(_rectTransform, false);
                _bars[unitId] = bar;
            }

            return bar;
        }
        
    }

}