using Tanks.UI;
using UnityEngine;

namespace Tanks.View.UI
{
    public class UiScreensManager
    {
        private Camera _camera;
        public Camera Camera => _camera;

        private UiFactory _uiFactory;
        private CanvasController _canvas;
        public CanvasController Canvas => _canvas;

        private HealthBarScreen _healthBarScreen;
        public HealthBarScreen HealthBarScreen => _healthBarScreen;

        public void Init(UiFactory uiFactory, CanvasController canvas)
        {
            _uiFactory = uiFactory;
            _canvas = canvas;
            
            _camera = Camera.main;
            
            CreateUiScreens();
            
            _healthBarScreen.Init(_uiFactory, _canvas);
        }

        public void Deinit()
        {
            _healthBarScreen.Deinit();
        }
        
        private void CreateUiScreens()
        {

            if (_healthBarScreen == null)
            {
                _healthBarScreen = _uiFactory.CreateHealthBarScreen();
                _healthBarScreen.transform.SetParent(_canvas.transform, false);
            }
        }

    }
}