using Tanks.Controller;

namespace Tanks.View
{
    public class CollisionsManager 
    {
        private ViewCollections _viewCollections;
        private GameController _gameController;
        
        public void Init(ViewCollections viewCollections, GameController gameController)
        {
            _viewCollections = viewCollections;
            _gameController = gameController;

            ManageSubscriptions(true);
        }

        public void Deinit()
        {
            ManageSubscriptions(false);
        }

        public void ManageSubscriptions(bool isActive)
        {
            var playerView = _viewCollections.Player;
            if (playerView != null)
            {
                if (isActive)
                {
                    playerView.OnCollided += OnPlayerCollided;
                }
                else
                {
                    playerView.OnCollided -= OnPlayerCollided; 
                }
            }
        }

        private void OnPlayerCollided(int playerId, int enemyId, bool isActive)
        {
            _gameController.RegisterTouch(enemyId, isActive);
        }
    }
}