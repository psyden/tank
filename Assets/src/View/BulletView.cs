using System;
using Tanks.Common;
using UnityEngine;

namespace Tanks.View
{
    public class BulletView : MonoBehaviour
    {
        private int _id;
        public int Id => _id;

        private string _weaponTypeName;
        public string WeaponTypeName => _weaponTypeName;
        
        private IBulletMovement _bulletMovement;
        public IBulletMovement BulletMovement => _bulletMovement;

        public event Action<int, int, Vector3> OnCollided = delegate(int bulletId, int enemyId, Vector3 position) {  }; 
        public event Action<int> OnWallCollided = delegate(int bulletId) {  }; 

        public void Init(int id, string weaponTypeName, IBulletMovement bulletMovement)
        {
            _id = id;
            _weaponTypeName = weaponTypeName;
            _bulletMovement = bulletMovement;
        }

        public void Deinit()
        {
            _bulletMovement.Deinit();
        }

        private void OnTriggerEnter(Collider other)
        {
            var enemyView = other.GetComponent<UnitView>();
            if (enemyView != null)
            {
                if (enemyView.Characteristics.IsEnemy)
                {
                    OnCollided?.Invoke(_id, enemyView.Id, transform.position);
                }
            }

            bool isWallCollision = other.gameObject.layer == LayerMask.NameToLayer(LayerName.Wall);
            if (isWallCollision)
            {
                OnWallCollided?.Invoke(_id);
            }
        }
    }
}