﻿using Tanks.Common;
using Tanks.Root;
using UnityEngine;

namespace Tanks.View
{
    public interface IBulletMovement
    {
        void Init(Transform bulletTransform, WeaponCharacteristics weaponCharacteristics);
        void Deinit();
        void Tick(DeltaTimeStruct dt);
    }

    public class StraightBulletMovement : IBulletMovement
    {
        private Transform _transform;
        private WeaponCharacteristics _weaponCharacteristics;
        private float _bulletSpeed;

        public void Init(Transform bulletTransform, WeaponCharacteristics weaponCharacteristics)
        {
            _transform = bulletTransform;
            _weaponCharacteristics = weaponCharacteristics;
            _bulletSpeed = _weaponCharacteristics.BulletSpeed;
        }

        public void Deinit()
        {
            
        }

        public void Tick(DeltaTimeStruct dt)
        {
            var deltaMovement = _bulletSpeed * dt.Delta * _transform.forward;
            _transform.position += deltaMovement;
        }
    }
}