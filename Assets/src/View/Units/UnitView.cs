using Tanks.Root;
using UnityEngine;

namespace Tanks.View
{
    public class UnitView : MonoBehaviour
    {

        [SerializeField]
        private Animator _animator;
        
        private int _id;
        public int Id => _id;

        private UnitCharacteristics _characteristics;
        public UnitCharacteristics Characteristics => _characteristics;
        
        public virtual void Init(int id, UnitCharacteristics characteristics)
        {
            _id = id;
            _characteristics = characteristics;
        }

        public virtual void Deinit()
        {
            transform.position = Vector3.down * 100;
        }

        public virtual void UpdateAnimation(float speedPercent)
        {
            _animator.SetFloat(AnimationNames.Speed, speedPercent);
        }
    }

    public static class AnimationNames
    {
        public const string Speed = "Speed";
    }
}