using System;
using UnityEngine;

namespace Tanks.View
{
    public class PlayerView : UnitView
    {
        [SerializeField]
        private Rigidbody _rb;
        public Rigidbody Rb => _rb;
        
        public event Action<int, int, bool> OnCollided = delegate(int unitId, int enemyId, bool isActive) {  };

        private void OnCollisionEnter(Collision other)
        {
            var enemyView = other.gameObject.GetComponent<UnitView>();
            if (enemyView != null)
            {
                if (enemyView.Characteristics.IsEnemy)
                {
                    OnCollided?.Invoke(Id, enemyView.Id, true);
                }
            }
        }
        
        private void OnCollisionExit(Collision other)
        {
            var enemyView = other.gameObject.GetComponent<UnitView>();
            if (enemyView != null)
            {
                if (enemyView.Characteristics.IsEnemy)
                {
                    OnCollided?.Invoke(Id, enemyView.Id, false);
                }
            }
        }
    }

}