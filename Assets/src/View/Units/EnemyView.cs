using Tanks.Root;
using UnityEngine;
using UnityEngine.AI;

namespace Tanks.View
{
    public class EnemyView : UnitView
    {
        [SerializeField]
        private NavMeshAgent _navAgent;
        
        public NavMeshAgent NavAgent => _navAgent;

        public override void Init(int id, UnitCharacteristics characteristics)
        {
            base.Init(id, characteristics);
            NavAgent.enabled = true;
        }

        public override void Deinit()
        {
            NavAgent.enabled = false;
            base.Deinit();
        }
    }
}