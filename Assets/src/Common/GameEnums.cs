namespace Tanks.Common
{
    public enum UnitFactoryType
    {
        Empty = 0,
        Player = 1,
        Enemy = 2,
    }
}