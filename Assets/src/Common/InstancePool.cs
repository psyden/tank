using System.Collections.Generic;

namespace Tanks.Common
{
    public class InstancePool<T>
    {
        private Stack<T> _items;

        public int Count => _items.Count;
        
        public InstancePool()
        {
            _items = new Stack<T>(10);
        }

        public T Pop()
        {
            T instance = _items.Pop();
            return instance;
        }

        public void Push(T instance)
        {
            _items.Push(instance);
        }
    }
}