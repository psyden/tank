using System.Collections.Generic;

namespace Tanks.Common
{
    public interface IUsed
    {
        void Use();
    }
    
    public class UsableList <T> : List<T> where T : IUsed
    {
        public UsableList(int capacity) : base(capacity)
        {
        }

        public T GetAndUse(int index)
        {
            T item = this[index];
            item.Use();
            this[index] = item; 
            return item;
        }
    }
}