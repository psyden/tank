using UnityEngine;

namespace Tanks.Common
{
    public static class GameMath
    {
        public static float CalculateDamage(float damage, float armor)
        {
            var resultDamage = damage * (1 - armor);
            return resultDamage;
        }
        
        public static Vector2 WorldToCanvasPosition(Vector3 position, Camera camera, RectTransform rectTransform)
        {
            Vector2 viewportPosition = camera.WorldToViewportPoint(position);
            
            Vector2 resultPosition = new Vector2(
                ((viewportPosition.x*rectTransform.sizeDelta.x)-(rectTransform.sizeDelta.x*0.5f)),
                ((viewportPosition.y*rectTransform.sizeDelta.y)-(rectTransform.sizeDelta.y*0.5f))
            );
            
            //Debug.Log($"roman: {resultPosition}");

            return resultPosition;
        }
    }
}