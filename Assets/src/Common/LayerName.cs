namespace Tanks.Common
{
    public static class LayerName
    {
        public const string Character = "Character";
        public const string Wall = "Wall";
    }
}