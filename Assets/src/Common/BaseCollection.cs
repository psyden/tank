using System.Collections;
using System.Collections.Generic;

namespace Tank.Common
{
    public class BaseCollection<T> : IEnumerable<T>
    {
        private Dictionary<int, T> _items;

        public BaseCollection()
        {
            _items = new Dictionary<int, T>();
        }

        public void Add(int id, T item)
        {
            _items[id] = item;
        }

        public void Remove(int id)
        {
            _items.Remove(id);
        }

        public bool Contains(int id)
        {
            return _items.ContainsKey(id);
        }

        public T Get(int id)
        {
            _items.TryGetValue(id, out T value);
            return value;
        }
        
        public int Count => _items.Count;
        public IEnumerator<T> GetEnumerator()
        {
            return _items.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

}