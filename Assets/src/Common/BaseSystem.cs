namespace Tanks.Common
{
    public class BaseSystem
    {
        public virtual void Start()
        {
            
        }

        public virtual void Stop()
        {
            
        }
    }
}