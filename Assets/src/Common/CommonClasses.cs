namespace Tanks.Common
{
    public struct DeltaTimeStruct
    {
        public readonly float DeltaTime;
        public readonly float TimeScale;

        public float Delta { get; private set; }

        public DeltaTimeStruct(float deltaTime, float timeScale)
        {
            DeltaTime = deltaTime;
            TimeScale = timeScale;

            Delta = deltaTime * timeScale;
        }
    }

    public enum UnitTypeEnum
    {
        Empty = 0,
        Player = 1,
        Enemy = 2
    }
}