using System.Collections.Generic;
using UnityEngine;

namespace Tanks.Root
{
    [CreateAssetMenu(fileName = "CharacteristicsConfigs", menuName = "Configs/CharacteristicsConfigs")]
    public class CharacteristicsConfigs : ScriptableObject
    {
        public List<UnitCharacteristicsConfig> UnitsConfigs;
        public List<WeaponCharacteristicsConfig> WeaponsConfigs;

        public Dictionary<string, UnitCharacteristicsConfig> _unitsCache;
        public Dictionary<string, WeaponCharacteristicsConfig> _weaponsCache;
        
        public UnitCharacteristicsConfig GetUnit(string unitTypeName)
        {
            if (_unitsCache == null)
            {
                InitCache();
            }

            _unitsCache.TryGetValue(unitTypeName, out var cfg);

            return cfg;
        }
        
        public WeaponCharacteristicsConfig GetWeapon(string weaponTypeName)
        {
            if (_weaponsCache == null)
            {
                InitCache();
            }

            _weaponsCache.TryGetValue(weaponTypeName, out var cfg);

            return cfg;
        }

        void InitCache()
        {
            _unitsCache = new Dictionary<string, UnitCharacteristicsConfig>();
            for (int i = 0; i < UnitsConfigs.Count; i++)
            {
                var cfg = UnitsConfigs[i];
                _unitsCache[cfg.Characteristics.UnitTypeName] = cfg;
            }
            
            _weaponsCache = new Dictionary<string, WeaponCharacteristicsConfig>();
            for (int i = 0; i < WeaponsConfigs.Count; i++)
            {
                var cfg = WeaponsConfigs[i];
                _weaponsCache[cfg.Characteristics.WeaponTypeName] = cfg;
            }
        }

    }
}