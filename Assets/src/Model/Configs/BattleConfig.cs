using UnityEngine;

namespace Tanks.Root
{
    
    [CreateAssetMenu(fileName = "BattleConfig", menuName = "Configs/BattleConfig")]
    public class BattleConfig : ScriptableObject
    {
        public int EnemiesQuantity = 10;
        public float GameFieldRadius = 15f;
    }
}