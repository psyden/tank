using System;
using UnityEngine;

namespace Tanks.Root
{
    [CreateAssetMenu(fileName = "Unit", menuName = "Configs/Unit")]
    public class UnitCharacteristicsConfig : ScriptableObject
    {
        public UnitCharacteristics Characteristics;

        public UnitCharacteristics GetClone()
        {
            return Characteristics.GetClone();
        }
    }

    [Serializable]
    public class UnitCharacteristics
    {
        public string UnitTypeName;
        
        public float Health;
        public float HealthMax;
        public float Armor;
        public float Speed;
        public float RotationSpeed;
        public bool IsEnemy;

        public UnitCharacteristics GetClone()
        {
            return new UnitCharacteristics()
            {
                UnitTypeName = UnitTypeName,
                Health = Health,
                HealthMax = HealthMax,
                Armor = Armor,
                Speed = Speed,
                RotationSpeed = RotationSpeed,
                IsEnemy = IsEnemy
            };
        }
    }
}