namespace Tanks.Root
{
    public static class AllGameConfigs
    {
        public static BattleConfig BattleCfg { get; set; }
        public static CharacteristicsConfigs CharacteristicsCfg { get; set; }
        
        public static void Init(BattleConfig battleCfg, CharacteristicsConfigs characteristicsCfg)
        {
            BattleCfg = battleCfg;
            CharacteristicsCfg = characteristicsCfg;
        }

    }
}