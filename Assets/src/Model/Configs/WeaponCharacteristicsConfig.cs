using System;
using UnityEngine;

namespace Tanks.Root
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Configs/Weapon")]
    public class WeaponCharacteristicsConfig : ScriptableObject
    {
        public WeaponCharacteristics Characteristics;

        public WeaponCharacteristics GetClone()
        {
            return Characteristics.GetClone();
        }
    }

    [Serializable]
    public struct WeaponCharacteristics
    {
        public string WeaponTypeName;
        public float Damage;
        public float DamageRadius;
        public float Frequency;
        public float BulletSpeed;

        public WeaponCharacteristics GetClone()
        {
            return new WeaponCharacteristics()
            {
                WeaponTypeName = WeaponTypeName,
                Damage = Damage,
                DamageRadius = DamageRadius,
                Frequency = Frequency,
                BulletSpeed = BulletSpeed
            };
        }
    }
}