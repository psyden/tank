using System.Collections.Generic;
using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;

namespace Tanks.Model
{
    public class GameModel
    {
        private GameController _gameController;
        private CharacteristicsConfigs _characteristicsConfigs;
        private Scheduler _scheduler;

        private ModelCollections _modelCollections;

        private List<BaseSystem> _systems;

        public void Init(GameController gameController, Scheduler scheduler)
        {
            _gameController = gameController;
            _scheduler = scheduler;
        }
        
        public void Start()
        {
            _modelCollections = new ModelCollections();
            _modelCollections.Init();
            
            var spawningSystem = new SpawningSystem();
            spawningSystem.Init(_modelCollections, _gameController);
            
            var damageSystem = new DamageSystem();
            damageSystem.Init(_modelCollections, _gameController);

            var weaponSystem = new WeaponSystem();
            weaponSystem.Init(_modelCollections, _gameController);

            _systems = new List<BaseSystem>
            {
                spawningSystem,
                damageSystem,
                weaponSystem
            };
            
            for (int i = 0; i < _systems.Count; i++)
            {
                var system = _systems[i];
                system.Start();
                if (system is IUpdate iUpdateSystem)
                {
                    _scheduler.AddUpdatable(iUpdateSystem);
                }
                if (system is ILateUpdate iLateUpdateSystem)
                {
                    _scheduler.AddUpdatable(iLateUpdateSystem);
                }
                if (system is IFixedUpdate iFixedUpdateSystem)
                {
                    _scheduler.AddUpdatable(iFixedUpdateSystem);
                }
            }

            _gameController.GameOverModel += OnGameOver;
        }

        public void Stop()
        {
            _gameController.GameOverModel -= OnGameOver;
            
            for (int i = 0; i < _systems.Count; i++)
            {
                var system = _systems[i];
                system.Stop();
                if (system is IUpdate iUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iUpdateSystem);
                }
                if (system is ILateUpdate iLateUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iLateUpdateSystem);
                }
                if (system is IFixedUpdate iFixedUpdateSystem)
                {
                    _scheduler.RemoveUpdatable(iFixedUpdateSystem);
                }
            }
        }

        private void OnGameOver()
        {
            Stop();
        }

        private static int _idCounter = 0;
        public static int GetUniqId()
        {
            _idCounter++;
            return _idCounter;
        }
    }
}