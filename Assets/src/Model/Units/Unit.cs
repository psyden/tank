using Tanks.Root;

namespace Tanks.Model
{
    public class Unit : IUnitCharacteristics
    {
        private int _id;
        public int Id => _id;

        private UnitCharacteristics _characteristics;
        
        public void Init(int id, UnitCharacteristics characteristics)
        {
            _id = id;
            _characteristics = characteristics;
        }

        public UnitCharacteristics GetUnitCharacteristics()
        {
            return _characteristics;
        }
    }

    public interface IUnitCharacteristics
    {
        UnitCharacteristics GetUnitCharacteristics();
    }
}