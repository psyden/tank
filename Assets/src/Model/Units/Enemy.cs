using Tanks.Model.Weapons;

namespace Tanks.Model
{
    public class Enemy : Unit
    {
        private IWeapon _weaponTouch;
        public IWeapon WeaponTouch => _weaponTouch;
        
        public void SetWeaponTouch(IWeapon weaponTouch)
        {
            _weaponTouch = weaponTouch;
        }
    }
}