using System.Collections.Generic;
using Tanks.Model.Weapons;

namespace Tanks.Model
{
    public class Player : Unit
    {
        private List<IWeapon> _weapons;
        public List<IWeapon> Weapons => _weapons;

        private int _currentWeaponIndex = -1;
        
        public IWeapon GetCurrentWeapon()
        {
            return _weapons[_currentWeaponIndex];
        }

        public IWeapon GetWeapon(string weaponTypeName)
        {
            IWeapon result = null;
            for (int i = 0; i < _weapons.Count; i++)
            {
                var w = _weapons[i];
                if (w.WeaponTypeName == weaponTypeName)
                {
                    result = w;
                    break;
                }
            }

            return result;
        }

        public void SwitchWeapon(bool nextElsePrev)
        {
            var weaponsCount = _weapons.Count - 1;
            if (nextElsePrev)
            {
                if (_currentWeaponIndex < weaponsCount)
                {
                    _currentWeaponIndex++;
                }
                else
                {
                    _currentWeaponIndex = 0;
                }
            }
            else
            {
                if (_currentWeaponIndex > 0)
                {
                    _currentWeaponIndex--;
                }
                else
                {
                    _currentWeaponIndex = weaponsCount;
                }
            }
        }

        public void SetWeapon(List<IWeapon> weapons)
        {
            _weapons = weapons;
            _currentWeaponIndex = 0;
        }
    }
}