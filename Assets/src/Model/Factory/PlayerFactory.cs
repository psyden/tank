using System.Collections.Generic;
using Tanks.Model.Weapons;
using Tanks.Root;

namespace Tanks.Model.Factory
{
    public class PlayerFactory : BaseUnitFactory
    {
        
        private List<string> _availableWeapons = new List<string>
        {
            WeaponTypeNames.WeaponFast,
            WeaponTypeNames.WeaponHeavy,
        };
        
        public Player Create(int id, UnitCharacteristicsConfig characteristicsCfg)
        {
            var player = new Player();
            var characteristics = characteristicsCfg.GetClone();
            player.Init(id, characteristics);

            var weapons = CreateWeapons();
            player.SetWeapon(weapons);

            return player;
        }

        List<IWeapon> CreateWeapons()
        {
            var data = new List<IWeapon>(5);
            
            var configs = AllGameConfigs.CharacteristicsCfg;
            for (int i = 0; i < _availableWeapons.Count; i++)
            {
                var weaponTypeName = _availableWeapons[i];
                var characteristicsCfg = configs.GetWeapon(weaponTypeName);
                var characteristics = characteristicsCfg.GetClone();

                var weapon = new Weapon();
                weapon.Init(characteristics);
                data.Add(weapon);
            }

            return data;
        }
    }
}