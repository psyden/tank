using Tanks.Model.Weapons;
using Tanks.Root;

namespace Tanks.Model.Factory
{
    public class EnemyFactory : BaseUnitFactory
    {
        public Enemy Create(int id, UnitCharacteristicsConfig characteristicsCfg)
        {
            var unit = new Enemy();
            var characteristics = characteristicsCfg.GetClone();
            unit.Init(id, characteristics);

            var touchWeapon = CreateTouchWeapon();
            unit.SetWeaponTouch(touchWeapon);
            
            return unit;
        }

        private IWeapon CreateTouchWeapon()
        {
            var weaponConfig = 
                AllGameConfigs.CharacteristicsCfg.GetWeapon(WeaponTypeNames.WeaponTouch);
            
            var weapon = new Weapon();
            var characteristics = weaponConfig.GetClone();
            weapon.Init(characteristics);
            
            return weapon;
        }
    }
}