using System.Collections.Generic;
using Tank.Common;

namespace Tanks.Model
{
    public class ModelCollections
    {
        private BaseCollection<Enemy> _enemies;
        private Player _player;

        private HashSet<int> _touchedEnemy;
        public HashSet<int> TouchedEnemy => _touchedEnemy;


        public BaseCollection<Enemy> Enemies => _enemies;

        public Player Player
        {
            get => _player;
            set => _player = value;
        }

        public void Init()
        {
            _enemies = new BaseCollection<Enemy>();
            _touchedEnemy = new HashSet<int>();
        }
    }
}