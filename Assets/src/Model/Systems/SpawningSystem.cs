using System.Collections.Generic;
using Tanks.Common;
using Tanks.Controller;
using Tanks.Model.Factory;
using Tanks.Root;
using UnityEngine;

namespace Tanks.Model
{
    public class SpawningSystem : BaseSystem, IUpdate
    {
        private ModelCollections _modelCollections;
        private GameController _gameController;
        
        private PlayerFactory _playerFactory;
        private EnemyFactory _enemyFactory;

        private List<string> _possibleEnemyNames;

        public void Init(ModelCollections modelCollections, GameController gameController)
        {
            _modelCollections = modelCollections;
            _gameController = gameController;

            _playerFactory = new PlayerFactory();
            _enemyFactory = new EnemyFactory();
            _possibleEnemyNames = new List<string>
            {
                UnitTypeNames.EnemySlow,
                UnitTypeNames.EnemyMedium,
                UnitTypeNames.EnemyFast,
            };
        }

        public override void Start()
        {
            base.Start();
            SpawnPlayer();
        }

        void SpawnPlayer()
        {
            var id = GameModel.GetUniqId();
            var unitTypeName = UnitTypeNames.Player;
            var characteristics = AllGameConfigs.CharacteristicsCfg.GetUnit(unitTypeName);
            var player = _playerFactory.Create(id, characteristics);

            _modelCollections.Player = player;
            
            _gameController.RegisterUnitSpawn(id, UnitFactoryType.Player, unitTypeName, Vector3.zero);
        }

        void SpawnEnemy()
        {
            var id = GameModel.GetUniqId();

            var index = Random.Range(0, _possibleEnemyNames.Count);
            var unitTypeName = _possibleEnemyNames[index];
            var characteristics = AllGameConfigs.CharacteristicsCfg.GetUnit(unitTypeName);
            var enemy = _enemyFactory.Create(id, characteristics);
            
            _modelCollections.Enemies.Add(id, enemy);

            Vector3 enemyPosition = CalculateEnemyPosition();
            _gameController.RegisterUnitSpawn(id, UnitFactoryType.Enemy, unitTypeName, enemyPosition);
        }

        private Vector3 CalculateEnemyPosition()
        {
            var radius = AllGameConfigs.BattleCfg.GameFieldRadius;

            var angle = Random.Range(-180, +180);
            var rotation = Quaternion.AngleAxis(angle, Vector3.up);
            var position = rotation * Vector3.forward * (radius + 1);

            return position;
        }

        public void OnUpdate(DeltaTimeStruct dt)
        {
            if (_modelCollections.Enemies.Count < AllGameConfigs.BattleCfg.EnemiesQuantity)
            {
                SpawnEnemy();
            }
        }
        
    }
}