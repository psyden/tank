using Tanks.Common;
using Tanks.Controller;
using Tanks.Model.Weapons;
using Tanks.Root;

namespace Tanks.Model
{
    public class DamageSystem : BaseSystem, IFixedUpdate
    {
        private ModelCollections _modelCollections;
        private GameController _gameController;

        public void Init(ModelCollections modelCollections, GameController gameController)
        {
            _modelCollections = modelCollections;
            _gameController = gameController;
        }

        public void OnFixedUpdate(DeltaTimeStruct dt)
        {
            ManageBulletHits();

            ManageTouches();
            ManageToucheDamage();
        }

        void ManageTouches()
        {
            var touches = _gameController.ReadTouch();

            for (int i = 0; i < touches.Count; i++)
            {
                var touch = touches[i];

                var enemyId = touch.EnemyId;
                bool isActive = touch.IsActive;

                if (isActive)
                {
                    _modelCollections.TouchedEnemy.Add(enemyId);
                }
                else
                {
                    _modelCollections.TouchedEnemy.Remove(enemyId);
                }
            }
            
            touches.Clear();
        }

        void ManageBulletHits()
        {
            var hits = _gameController.GetBulletHits();

            for (int i = 0; i < hits.Count; i++)
            {
                var hit = hits[i];
                var bulletId = hit.BulletId;
                var weaponTypeName = hit.WeaponTypeName;
                var enemies = hit.Enemies;

                IWeapon weapon = _modelCollections.Player.GetWeapon(weaponTypeName);
                var weaponDamage = weapon.Characteristics.Damage;

                for (int enemyIndex = 0; enemyIndex < enemies.Count; enemyIndex++)
                {
                    int enemyId = enemies[enemyIndex];
                    
                    Enemy enemy = _modelCollections.Enemies.Get(enemyId);
                    if (enemy != null)
                    {
                        var characteristics = enemy.GetUnitCharacteristics();
                        var health = characteristics.Health;
                        var armor = characteristics.Armor;
                    
                        var dmg = GameMath.CalculateDamage(weaponDamage, armor);
                        var resultDamage = (dmg > health) ? health : dmg;
                    
                        var resultHealth = health - resultDamage;
                        characteristics.Health = resultHealth;
                    
                        bool isDead = resultHealth <= 0;

                        _gameController.RegisterDamage(enemyId, UnitTypeEnum.Enemy, resultDamage, resultHealth, isDead);

                        if (isDead)
                        {
                            KillEnemy(enemyId);
                        }
                    }
                }
                
            }
            
            hits.Clear();
        }

        void KillEnemy(int id)
        {
            _modelCollections.Enemies.Remove(id);
            _modelCollections.TouchedEnemy.Remove(id);
        }

        void ManageToucheDamage()
        {
            var touchedEnemy = _modelCollections.TouchedEnemy;
            
            foreach (var enemyId in touchedEnemy)
            {
                var enemy = _modelCollections.Enemies.Get(enemyId);
                if (enemy != null)
                {
                    var weapon = enemy.WeaponTouch;
                    var touchWeaponIsReady = weapon.IsReady();

                    if (touchWeaponIsReady)
                    {
                        weapon.Reset();
                    
                        var playerCharacteristics = _modelCollections.Player.GetUnitCharacteristics();
                        var health = playerCharacteristics.Health;
                        var armor = playerCharacteristics.Armor;
                    
                        var weaponDamage = weapon.Characteristics.Damage;
                        var dmg = GameMath.CalculateDamage(weaponDamage, armor);
                        var resultDamage = (dmg > health) ? health : dmg;
                    
                        var resultHealth = health - resultDamage;
                        playerCharacteristics.Health = resultHealth;
                    
                        bool playerIsDead = resultHealth <= 0;

                        var playerId = _modelCollections.Player.Id;
                        _gameController.RegisterDamage(playerId, UnitTypeEnum.Player, resultDamage, resultHealth, playerIsDead);

                        if (playerIsDead)
                        {
                            _gameController.RegisterGameOver();
                        }
                    }
                }
            }
        }

    }
}