using Tanks.Common;
using Tanks.Controller;
using Tanks.Root;

namespace Tanks.Model
{
    public class WeaponSystem : BaseSystem, IUpdate
    {
        private ModelCollections _modelCollections;
        private GameController _gameController;

        private bool _shootIsActive = false;

        public void Init(ModelCollections modelCollections, GameController gameController)
        {
            _modelCollections = modelCollections;
            _gameController = gameController;
        }

        public void OnUpdate(DeltaTimeStruct dt)
        {
            UpdateWeaponsTimers(dt);
            ManageWeaponSwitching();
            ManageShootRequests();
            ManageTouchRequests();
        }

        private void ManageWeaponSwitching()
        {
            var switchingValues = _gameController.ReadChangeWeaponRequest();

            for (int i = 0; i < switchingValues.Count; i++)
            {
                var value = switchingValues[i];
                _modelCollections.Player.SwitchWeapon(value);
            }
            
            switchingValues.Clear();
        }

        private void UpdateWeaponsTimers(DeltaTimeStruct dt)
        {
            var player = _modelCollections.Player;
            if (player != null)
            {
                var weapons = player.Weapons;
                for (int i = 0; i < weapons.Count; i++)
                {
                    var weapon = weapons[i];
                    weapon.Tick(dt);
                }
            }

            var enemies = _modelCollections.Enemies;
            foreach (var enemy in enemies)
            {
                enemy.WeaponTouch.Tick(dt);
            }
        }
        
        private void ManageShootRequests()
        {
            var requests = _gameController.GetShootButtonChange();
            
            var player = _modelCollections.Player;
            if (player != null)
            {
                for (int i = 0; i < requests.Count; i++)
                {
                    var reqData = requests[i];
                
                    _shootIsActive = reqData.Shoot;
                    if (_shootIsActive)
                    {
                        var currentWeapon = player.GetCurrentWeapon();
                        if (currentWeapon.IsReady())
                        {
                            currentWeapon.Reset();
                            var id = GameModel.GetUniqId();
                            _gameController.RegisterShootAnswer(id, true, currentWeapon.WeaponTypeName, reqData.Position, reqData.Direction);
                        }

                    }
                }
            }
            
            requests.Clear();
        }
        
        private void ManageTouchRequests()
        {
        }

        
    }
}