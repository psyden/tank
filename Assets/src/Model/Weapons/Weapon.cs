using Tanks.Common;
using Tanks.Root;

namespace Tanks.Model.Weapons
{
    public class Weapon : IWeapon
    {
        private WeaponCharacteristics _characteristics;
        public WeaponCharacteristics Characteristics => _characteristics;

        public string WeaponTypeName => _characteristics.WeaponTypeName;
        
        private float _timer;

        public void Init(WeaponCharacteristics characteristics)
        {
            _characteristics = characteristics;
            _timer = 0;
        }

        public bool IsReady()
        {
            return _timer <= 0;
        }

        public void Reset()
        {
            _timer = _characteristics.Frequency;
        }

        public void Tick(DeltaTimeStruct dt)
        {
            _timer -= dt.Delta;
        }
    }

    public interface IWeapon
    {
        WeaponCharacteristics Characteristics { get; }
        string WeaponTypeName { get; }
        bool IsReady();
        void Reset();
        void Tick(DeltaTimeStruct dt);
    }
}