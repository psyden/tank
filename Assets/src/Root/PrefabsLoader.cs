using System;
using System.Collections.Generic;
using Tanks.Common;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Tanks.Root
{
    public class PrefabsLoader : IUpdate
    {
        private Scheduler _scheduler;
        private Action _loadingCallback;

        private Dictionary<string, LoadingChunkStruct> _loadingChunks;
        private Dictionary<string, GameObject> _prefabs;

        public PrefabsLoader(Scheduler scheduler)
        {
            _scheduler = scheduler;
            _loadingChunks = new Dictionary<string, LoadingChunkStruct>();
            _prefabs = new Dictionary<string, GameObject>();
        }

        public void LoadPrefabs(string chunkId, List<string> prefabNames, Action successCb = null,
            Action errorCb = null)
        {

            if (_loadingChunks.ContainsKey(chunkId))
            {
                UnloadPrefabs(chunkId);
                _scheduler.RemoveUpdatable(this);
            }
            
            var chunk = new LoadingChunkStruct()
            {
                ChunkId = chunkId,
                SuccessCallback = successCb,
                ErrorCallback = errorCb
            };

            _loadingChunks.Add(chunkId, chunk);

            for (int index = 0; index < prefabNames.Count; index++)
            {
                string name = prefabNames[index];

                chunk.PrefabsId.Add(name);
                chunk.Operations.Add(Addressables.LoadAssetAsync<GameObject>(name));
            }

            _scheduler.AddUpdatable(this);
        }

        public bool AddressableAssetExists(object key)
        {
            foreach (var l in Addressables.ResourceLocators)
            {
                IList<IResourceLocation> locs;
                if (l.Locate(key, typeof(GameObject), out locs))
                    return true;
            }
            
            return false;
        }

        public void UnloadPrefabs(string chunkId)
        {
            if (_loadingChunks.TryGetValue(chunkId, out LoadingChunkStruct data))
            {
                for (int i = 0; i < data.Prefabs.Count; i++)
                {
                    Addressables.Release(data.Prefabs[i]);
                }
            }

            _loadingChunks.Remove(chunkId);
            Resources.UnloadUnusedAssets();
        }

        public GameObject GetPrefab(string prefabName)
        {
            _prefabs.TryGetValue(prefabName, out GameObject prefab);
            return prefab;
        }

        public void OnUpdate(DeltaTimeStruct dt)
        {
            int finishedChunksCurrent = 0;
            int finishedChunksTotal = _loadingChunks.Count;

            foreach (KeyValuePair<string, LoadingChunkStruct> chunkEntry in _loadingChunks)
            {
                LoadingChunkStruct chunk = chunkEntry.Value;
                if (!chunk.IsFinished)
                {
                    int quantityTotal = chunk.Operations.Count;
                    int quantityCurrent = 0;
                    int quantityError = -1;
                    for (int i = 0; i < quantityTotal; i++)
                    {
                        AsyncOperationHandle<GameObject> operation = chunk.Operations[i];
                        if (operation.Status == AsyncOperationStatus.Succeeded)
                        {
                            quantityCurrent += 1;
                        }
                        else if (operation.Status == AsyncOperationStatus.Failed)
                        {
                            quantityCurrent = quantityError;
                            break;
                        }
                    }

                    if (quantityCurrent == quantityError)
                    {
                        chunk.IsFinished = true;
                        chunk.ErrorCallback?.Invoke();
                    }
                    else if (quantityCurrent == quantityTotal)
                    {
                        chunk.IsFinished = true;

                        for (int i = 0; i < quantityTotal; i++)
                        {
                            AsyncOperationHandle<GameObject> operation = chunk.Operations[i];
                            GameObject prefab = operation.Result;
                            chunk.Prefabs.Add(prefab);

                            string prefabName = chunk.PrefabsId[i];
                            _prefabs[prefabName] = prefab;
                        }

                        chunk.SuccessCallback?.Invoke();
                    }
                }
                else
                {
                    finishedChunksCurrent += 1;
                }
            }

            if (finishedChunksCurrent == finishedChunksTotal)
            {
                _scheduler.RemoveUpdatable(this);
            }
        }
    }

    public class LoadingChunkStruct
    {
        public string ChunkId;
        public List<string> PrefabsId;
        public List<AsyncOperationHandle<GameObject>> Operations;
        public List<GameObject> Prefabs;
        public Action SuccessCallback;
        public Action ErrorCallback;
        public bool IsFinished;

        public LoadingChunkStruct()
        {
            PrefabsId = new List<string>();
            Operations = new List<AsyncOperationHandle<GameObject>>();
            Prefabs = new List<GameObject>();
            IsFinished = false;
        }
    }
}