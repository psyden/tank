namespace Tanks.Root
{
    public static class UnitTypeNames
    {
        public const string Player = "player";
        public const string EnemySlow = "enemy_slow";
        public const string EnemyMedium = "enemy_medium";
        public const string EnemyFast = "enemy_fast";
    }

    public static class WeaponTypeNames
    {
        public const string WeaponFast = "weapon_fast";
        public const string WeaponHeavy = "weapon_heavy";
        public const string WeaponTouch = "weapon_touch";
    }

    public static class BulletTypeNames
    {
        public const string WeaponFastBullet = "weapon_fast_bullet";
        public const string WeaponHeavyBullet = "weapon_heavy_bullet";
    }

    public static class UIPrefabsNames
    {
        public const string HealthScreen = "HealthScreen";
        public const string HealthBar = "HealthBar";
        public const string GameOverScreen = "GameOverScreen";
    }
}