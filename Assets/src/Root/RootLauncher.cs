﻿using System;
using System.Collections.Generic;
using Tanks.Controller;
using Tanks.Model;
using Tanks.UI;
using Tanks.View;
using UnityEngine;

namespace Tanks.Root
{
    public class RootLauncher : MonoBehaviour
    {
        [SerializeField]
        private CharacteristicsConfigs _characteristicsConfigs;
        [SerializeField]
        private BattleConfig _battleCfg;
        [SerializeField]
        private CanvasController _canvas;
        
        private Scheduler _scheduler;
        private PrefabsLoader _prefabsLoader;
        
        private GameController _controller;
        private GameModel _model;
        private GameView _view;

        void Start()
        {
            Init();
            StartGame();
        }

        void Init()
        {
            AllGameConfigs.Init(_battleCfg, _characteristicsConfigs);
            
            var schedulerGO = new GameObject("Scheduler");
            _scheduler = schedulerGO.AddComponent<Scheduler>();

            _prefabsLoader = new PrefabsLoader(_scheduler);

            _controller = new GameController();
            _model = new GameModel();
            _view = new GameView();

            _model.Init(_controller, _scheduler);
            _view.Init(_controller, _prefabsLoader, _scheduler, _canvas);
            _controller.Init(_model, _view, _scheduler);
        }

        void StartGame()
        {
            LoadPrefabs(OnPrefabsLoaded);
        }
        private void LoadPrefabs(Action successCb)
        {
            var prefabs = new List<string>
            {
                UnitTypeNames.Player,
                UnitTypeNames.EnemySlow,
                UnitTypeNames.EnemyMedium,
                UnitTypeNames.EnemyFast,
                BulletTypeNames.WeaponFastBullet,
                BulletTypeNames.WeaponHeavyBullet,
                
                UIPrefabsNames.HealthBar,
                UIPrefabsNames.HealthScreen,
                UIPrefabsNames.GameOverScreen,
            };
            _prefabsLoader.LoadPrefabs("units", prefabs, successCb);
        }

        private void OnPrefabsLoaded()
        {
            _model.Start();
            _view.Start();
        }
    }
}