using System.Collections.Generic;
using Tanks.Common;
using UnityEngine;

namespace Tanks.Root
{
    public class Scheduler : MonoBehaviour
    {
        private List<IUpdate> _updatableObjects = new List<IUpdate>();
        private List<IUpdate> _updatableObjectsCopy = new List<IUpdate>();
        
        private List<ILateUpdate> _lateUpdatableObjects = new List<ILateUpdate>();
        private List<ILateUpdate> _lateUpdatableObjectsCopy = new List<ILateUpdate>();
        
        private List<IFixedUpdate> _fixedUpdatableObjects = new List<IFixedUpdate>();
        private List<IFixedUpdate> _fixedUpdatableObjectsCopy = new List<IFixedUpdate>();
        
        public void AddUpdatable(IUpdate obj)
        {
            if (!_updatableObjects.Contains(obj))
            {
                _updatableObjects.Add(obj);
            }
        }
        
        public void AddUpdatable(ILateUpdate obj)
        {
            if (!_lateUpdatableObjects.Contains(obj))
            {
                _lateUpdatableObjects.Add(obj);
            }
        }
        
        public void AddUpdatable(IFixedUpdate obj)
        {
            if (!_fixedUpdatableObjects.Contains(obj))
            {
                _fixedUpdatableObjects.Add(obj);
            }
        }
        
        public void RemoveUpdatable(IUpdate obj)
        {
            if (_updatableObjects.Contains(obj))
            {
                _updatableObjects.Remove(obj);
            }
        }
        
        public void RemoveUpdatable(ILateUpdate obj)
        {
            if (_lateUpdatableObjects.Contains(obj))
            {
                _lateUpdatableObjects.Remove(obj);
            }
        }
        
        public void RemoveUpdatable(IFixedUpdate obj)
        {
            if (_fixedUpdatableObjects.Contains(obj))
            {
                _fixedUpdatableObjects.Remove(obj);
            }
        }

        public void ClearUpdatable()
        {
            _updatableObjects.Clear();
            _lateUpdatableObjects.Clear();
            _fixedUpdatableObjects.Clear();
        }

        void Update()
        {
            _updatableObjectsCopy.Clear();
            _updatableObjectsCopy.AddRange(_updatableObjects);
            
            for (int i = 0; i < _updatableObjectsCopy.Count; i++)
            {
                _updatableObjectsCopy[i]?.OnUpdate(new DeltaTimeStruct(Time.deltaTime, Time.timeScale));
            }
        }

        void LateUpdate()
        {
            _lateUpdatableObjectsCopy.Clear();
            _lateUpdatableObjectsCopy.AddRange(_lateUpdatableObjects);
            
            for (int i = 0; i < _lateUpdatableObjectsCopy.Count; i++)
            {
                _lateUpdatableObjectsCopy[i]?.OnLateUpdate(new DeltaTimeStruct(Time.deltaTime, Time.timeScale));
            }
        }
    
        void FixedUpdate()
        {
            _fixedUpdatableObjectsCopy.Clear();
            _fixedUpdatableObjectsCopy.AddRange(_fixedUpdatableObjects);
            
            for (int i = 0; i < _fixedUpdatableObjectsCopy.Count; i++)
            {
                _fixedUpdatableObjectsCopy[i]?.OnFixedUpdate(new DeltaTimeStruct(Time.fixedDeltaTime, Time.timeScale));
            }
        }
       
    }
    
    public interface IUpdate
    {
        void OnUpdate(DeltaTimeStruct dt);
    }

    public interface ILateUpdate
    {
        void OnLateUpdate(DeltaTimeStruct dt);
    }
    
    public interface IFixedUpdate
    {
        void OnFixedUpdate(DeltaTimeStruct dt);
    }
}