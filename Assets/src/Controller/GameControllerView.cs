using System;
using System.Collections.Generic;
using Tanks.Common;
using UnityEngine;

namespace Tanks.Controller
{
    public partial class GameController
    {
        #region unit spawn

        public UsableList<RegisterUnitSpawnStruct> GetUnitSpawns()
        {
            if (_unitSpawns == null)
            {
                _unitSpawns = new UsableList<RegisterUnitSpawnStruct>(10);
            }

            return _unitSpawns;
        }

        #endregion
        
        #region Shoot

        public List<ShootAnswer> GetShootAnswers()
        {
            if (_shootAnswers == null)
            {
                _shootAnswers = new List<ShootAnswer>(10);
            }

            return _shootAnswers;
        }

        private List<ShootButtonChangeStruct> _shootButtonChanges;
        public void RegisterShootButtonChange (bool shoot, Vector3 position, Vector3 direction)
        {
            if (_shootButtonChanges == null)
            {
                _shootButtonChanges = new List<ShootButtonChangeStruct>(10);
            }

            var data = new ShootButtonChangeStruct
            {
                Shoot = shoot,
                Position = position,
                Direction = direction,
            };
            
            _shootButtonChanges.Add(data);
        }

        public struct ShootButtonChangeStruct
        {
            public bool Shoot;
            public Vector3 Position;
            public Vector3 Direction;
        }

        #endregion

        #region SwitchWeapon

        private List<bool> _switchingWeaponsValue = new List<bool>(10);
        
        public void RegisterChangeWeaponRequest(bool switchingValue)
        {
            _switchingWeaponsValue.Add(switchingValue);
        }

        #endregion

        #region Bullet hit enemy

        private List<BulletHitStruct> _bulletHits;
        
        public void RegisterBulletHits(int bulletId, string weaponTypeName, List<int> enemies)
        {
            if (_bulletHits == null)
            {
                _bulletHits = new List<BulletHitStruct>(10);
            }

            var data = new BulletHitStruct
            {
                BulletId = bulletId,
                WeaponTypeName = weaponTypeName,
                Enemies = enemies
            };
            
            _bulletHits.Add(data);
        }

        public struct BulletHitStruct
        {
            public int BulletId;
            public string WeaponTypeName;
            public List<int> Enemies;
        }

        #endregion

        #region Damage

        public List<DamageStruct> GetDamage()
        {
            if (_damage == null)
            {
                _damage = new List<DamageStruct>(10);
            }

            return _damage;
        }

        #endregion

        #region Touch

        private List<TouchStruct> _touch;

        public void RegisterTouch(int enemyId, bool isActive)
        {

            if (_touch == null)
            {
                _touch = new List<TouchStruct>(10);
            }

            var data = new TouchStruct(enemyId, isActive);
            _touch.Add(data);
        }

        [Serializable]
        public struct TouchStruct
        {
            public readonly int EnemyId;
            public readonly bool IsActive;

            public TouchStruct(int enemyId, bool isActive)
            {
                EnemyId = enemyId;
                IsActive = isActive;
            }
        }

        #endregion

        #region GameOver

        

        #endregion
        
    }
}