using System;
using System.Collections.Generic;
using Tanks.Common;
using Tanks.Model;
using Tanks.Root;
using Tanks.View;
using UnityEngine;

namespace Tanks.Controller
{
    public partial class GameController
    {
        private GameModel _model;
        private GameView _view;
        private Scheduler _scheduler;

        public void Init(GameModel model, GameView view, Scheduler scheduler)
        {
            _model = model;
            _view = view;
            _scheduler = scheduler;
        }

        #region unit spawn

        private UsableList<RegisterUnitSpawnStruct> _unitSpawns;
        public void RegisterUnitSpawn(int id, UnitFactoryType factoryType, string unitTypeName, Vector3 position)
        {
            if (_unitSpawns == null)
            {
                _unitSpawns = new UsableList<RegisterUnitSpawnStruct>(10);
            }

            var register = new RegisterUnitSpawnStruct
            {
                Id = id,
                FactoryType = factoryType,
                UnitTypeName = unitTypeName,
                Position = position,
                IsUsed = false
            };

            _unitSpawns.Add(register);
        }
        
        #endregion

        #region Shoot

        public List<ShootButtonChangeStruct> GetShootButtonChange()
        {
            if (_shootButtonChanges == null)
            {
                _shootButtonChanges = new List<ShootButtonChangeStruct>(10);
            }

            return _shootButtonChanges;
        }

        private List<ShootAnswer> _shootAnswers;
        
        public void RegisterShootAnswer(int id, bool shoot, string weaponTypeName, Vector3 position, Vector3 direction)
        {
            if (_shootAnswers == null)
            {
                _shootAnswers = new List<ShootAnswer>(10);
            }

            var data = new ShootAnswer
            {
                Id = id,
                Shoot = shoot,
                WeaponTypeName = weaponTypeName,
                Position = position,
                Direction = direction
            };
            _shootAnswers.Add(data);
        }

        public struct ShootAnswer
        {
            public int Id;
            public bool Shoot;
            public string WeaponTypeName;
            public Vector3 Position;
            public Vector3 Direction;
        }

        #endregion
        
        #region SwitchWeapon
        
        public List<bool> ReadChangeWeaponRequest()
        {
            return _switchingWeaponsValue;
        }
        
        #endregion
        
        #region Bullet hit enemy

        public List<BulletHitStruct> GetBulletHits()
        {
            if (_bulletHits == null)
            {
                _bulletHits = new List<BulletHitStruct>(10);
            }

            return _bulletHits;
        }

        #endregion

        #region Damage

        private List<DamageStruct> _damage;

        public void RegisterDamage(int enemyId, UnitTypeEnum unitType, float damage, float health, bool isDead)
        {
            if (_damage == null)
            {
                _damage = new List<DamageStruct>(10);
            }

            var data = new DamageStruct
            {
                UnitId = enemyId,
                UnitType = unitType,
                Damage = damage,
                Health = health,
                IsDead = isDead
            };
            
            _damage.Add(data);
        }

        public struct DamageStruct
        {
            public int UnitId;
            public UnitTypeEnum UnitType;
            public float Damage;
            public float Health;
            public bool IsDead;
        }

        #endregion

        #region Touch

        public List<TouchStruct> ReadTouch()
        {
            if (_touch == null)
            {
                _touch = new List<TouchStruct>(10);
            }

            return _touch;
        }

        #endregion

        #region GameOver

        public event Action GameOverView = delegate { };
        public event Action GameOverModel = delegate { };
        
        public void RegisterGameOver()
        {
            GameOverView.Invoke();
            GameOverModel.Invoke();
        }

        #endregion
        
    }

    public struct RegisterUnitSpawnStruct : IUsed
    {
        public int Id;
        public UnitFactoryType FactoryType;
        public string UnitTypeName;
        public Vector3 Position;
        public bool IsUsed;

        public void Use()
        {
            IsUsed = true;
        }
    }
}